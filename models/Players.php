<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "players".
 *
 * @property int $id
 * @property string $name
 * @property string $lastname
 * @property string $firstname
 * @property string $dateofbirth
 * @property int $position
 * @property int $id_command
 */
class Players extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'players';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateofbirth'], 'safe'],
            [['position', 'id_command'], 'integer'],
            [['name', 'lastname'], 'string', 'max' => 50],
            [['firstname'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Імя',
            'lastname' => 'Прізвище',
            'firstname' => 'Імя',
            'dateofbirth' => 'Дата народження',
            'position' => 'Позиція',
            'id_command' => '',
        ];
    }
}
