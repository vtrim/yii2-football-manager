<?php

use yii\db\Migration;

/**
 * Handles the creation of table `players`.
 */
class m170305_090407_create_players_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'lastname' => $this->string(50),
            'firstname' => $this->string(50),
            'dateofbirth' => $this->date(),
            'position' => $this->integer(2),
            'id_command' => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('players');
    }
}
