<?php

use yii\db\Migration;

/**
 * Handles the creation of table `commands`.
 */
class m170305_073208_create_commands_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('commands', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'year' => $this->integer(5) 
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('commands');
    }
}
