<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Команди';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commands-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Додати команду', ['commands/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
    'label' => 'Назва',
    'format' => 'raw',
    'value' => function($model){
        return Html::a(
            $model->name,
            '?r=commands/view&id='.$model->id,
            [
                'title' => 'Редагувати',
                'target' => '_blank'
            ]
        );
    }
],
            'year',
            
            [
    'label' => 'Дія',
    'format' => 'raw',
    'value' => function($model){
        return Html::a(
            'Редагувати',
            '?r=commands/update&id='.$model->id,
            [
                'title' => 'Редагувати',
                'target' => '_blank'
            ]
        ) . Html::beginForm(['commands/delete', 'id' => $model->id], 'post')
        . Html::submitButton('Видалити', ['class' => 'submit'])
        . Html::endForm();
    }
],      

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
