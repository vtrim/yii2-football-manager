<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Гравці в - ' . $command_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="players-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Додати гравця', ['players/create', 'id' => $id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'id',
            'name',
            'lastname',
            'firstname',
            'dateofbirth',
            'position',
            // 'id_command', 
            
            [
    'label' => 'Дія',
    'format' => 'raw',
    'value' => function($model){
        return Html::a(
            'Редагувати',
            '?r=players/update&id='.$model->id,
            [
                'title' => 'Редагувати',
                'target' => '_blank'
            ]
        ) . Html::beginForm(['players/delete', 'id' => $model->id], 'post')
        . Html::submitButton('Видалити', ['class' => 'submit'])
        . Html::endForm();
    }
],

        
        ],         
        
    ]); ?>
</div>
