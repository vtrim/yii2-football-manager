<?php

namespace app\controllers;

use Yii;
use app\models\Commands;
use app\models\Players;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\grid\GridView;

/**
 * CommandsController implements the CRUD actions for Commands model.
 */
class CommandsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Commands model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$command_name = Commands::findOne($id);
    	if(!$command_name){
    		throw new NotFoundHttpException('Такої команди не існує');
    		}

        
    	$dataProvider = new ActiveDataProvider([
            'query' => Players::find()->where(['id_command'=>$id]),
        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'id'=>$id,
            'command_name'=>$command_name->name
        ]);
    }

    /**
     * Creates a new Commands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Commands();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['site/index']);
        } else {
            return $this->render('create.twig', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Commands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModelCommands($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['site/index']);
        } else {
            return $this->render('update.twig', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Commands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModelCommands($id)->delete();
        Players::deleteAll(['id_command'=>$id]);

        return $this->redirect(['site/index']);
    }

    /**
     * Finds the Commands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Commands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Players::find()->where(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
        protected function findModelCommands($id)
    {
        if (($model = Commands::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
